import 'dart:collection';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:thai_university/screen/home_register_screen.dart';
import 'package:thai_university/securestorage/secure_storage.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();

}

class _RegisterScreenState extends State<RegisterScreen> {

  final controllerName = TextEditingController();
  final controllerLastname = TextEditingController();
  final controllerAge = TextEditingController();
  final controllerIdCard = TextEditingController();
  final controllerPhoneNumber = TextEditingController();

  HashMap<String, dynamic> user = HashMap();

  String message = "";

  @override
  void initState() {
    getExistingUsername();
    super.initState();
  }

  Future getExistingUsername() async {
    HashMap account = await UserSecureStorage.getUser();
    if(account["name"].toString().length >= 5){
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) => const HomeRegisterScreen()));
    }
  }


  Future<void> onSubmit() async {
    setState(() {
      message = "";
    });
    String userName = controllerName.text;
    if(userName.length >= 5){
      String lastName = controllerLastname.text;
      int age = int.parse(controllerAge.text);
      String idCard = controllerIdCard.text;
      String phone = controllerPhoneNumber.text;

      user["name"] = userName;
      user["lastname"] = lastName;
      user["age"] = age;
      user["IdCard"] = idCard;
      user["phone"] = phone;


      await UserSecureStorage.setUser(user);
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) => const HomeRegisterScreen()));
    } else {
        setState(() {
          message = "Please input longer name >=5 ";
        });
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 0.0,
          backgroundColor: const Color.fromARGB(255, 255, 209, 0),
          leading: IconButton(
              onPressed: () {
                // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext Context) =>HomeScreen()));
              },
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.black,
              ))),
      body: Container(
        decoration: const BoxDecoration(color: Color.fromARGB(255, 255, 209, 0)),
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    Text(
                      'ยินดีต้อนรับ!',
                      style: TextStyle(
                        color: Color.fromARGB(255, 0, 15, 159),
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    Expanded(
                      child: Text(
                        'กรุณากรอกชื่อเเละหมายเลขโทรศัพท์มือถือเพื่อสร้างบัญชีชั่วคราวกับเงินไชโย',
                        style:
                        TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Container(
                  alignment: Alignment.topLeft,
                  child:  const Text(
                    'ชื่อ*',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  child: TextFormField(
                    controller: controllerName,
                    decoration: InputDecoration(fillColor: Colors.white, filled:true,
                      // hintText: 'Enter your Fristname',
                      labelText: 'กรอกชื่อ',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: const BorderSide(color: Colors.black),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  alignment: Alignment.topLeft,
                  child: const Text(
                    'นามสกุล*',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(height: 10),
                TextFormField(
                  controller: controllerLastname,
                  decoration: InputDecoration(fillColor: Colors.white, filled: true,
                    // hintText: 'Enter a Email',
                    labelText: 'กรอกนามสกุล',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: const BorderSide(color: Colors.black),
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  alignment: Alignment.topLeft,
                  child: const Text(
                    'อายุ*',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(height: 10),
                TextFormField(
                  controller: controllerAge,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(fillColor: Colors.white, filled: true,
                    // hintText: 'Enter a Email',
                    labelText: 'กรอกอายุ',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: const BorderSide(color: Colors.black),
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  alignment: Alignment.topLeft,
                  child: const Text(
                    'เลขประจำตัวประชาชน*',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(height: 10),
                TextField(
                  controller: controllerIdCard,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(fillColor: Colors.white, filled: true,
                    labelText: 'กรอกเลขบัตรประชาชน 13 หลัก',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: const BorderSide(color: Colors.black),
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  alignment: Alignment.topLeft,
                  child: const Text(
                    'กรอกหมายเลขโทรศัพท์มือถือ*',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(height: 10),
                TextField(
                  controller: controllerPhoneNumber,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(fillColor: Colors.white, filled: true,
                    // hintText: 'Enter a phone number',
                    labelText: 'กรอกหมายเลขโทรศัพท์มือถือ',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: const BorderSide(color: Colors.black),
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                RichText(
                  text: TextSpan(
                    text:
                    'โปรดอ่านเพิ่มเติมเกี่ยวกับประกาศนโยบายความเป็นส่วนตัวของเราอย่างละเอียดเพื่อเข้าใจถึงวิธีการที่เรารวบรวมใช้เเละเปิดเผยข้อมูลส่วนตัวบุคคลของคุณเเละสิทธิของคุณ  ',
                    style: const TextStyle(color: Colors.black),
                    children: [
                      TextSpan(
                        text: 'ที่นี่',
                        style: const TextStyle(color: Colors.red),
                        recognizer: TapGestureRecognizer()..onTap = () {},
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                Text(message,style: TextStyle(color: Colors.red),),
                SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: const Color.fromARGB(255, 0, 15, 159),
                    ),
                    onPressed: () {
                      onSubmit();
                    },
                    child: const Text('ยืนยัน'),
                  ),
                ),
                const SizedBox(height: 20),
                // InkWell(
                //   child: const Text('Chick Me To Test Navigator Please!!!!!'),
                //   onTap: (){
                //      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext Context) =>SplashScreen()));
                //   },
                // )
              ],
            ),
          ),
        ),
      ),
      // backgroundColor: const Color(0x90FFD100),
    );
  }
}
