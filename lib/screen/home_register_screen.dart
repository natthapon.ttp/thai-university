import 'package:flutter/material.dart';
import 'package:thai_university/screen/register_screen.dart';
import 'package:provider/provider.dart';
import 'package:thai_university/screen/thai_university_widget.dart';
import 'package:thai_university/securestorage/secure_storage.dart';
import '../providers/profile_provider.dart';

class HomeRegisterScreen extends StatefulWidget {
  const HomeRegisterScreen({Key? key}) : super(key: key);

  @override
  State<HomeRegisterScreen> createState() => _HomeRegisterScreenState();
}

class _HomeRegisterScreenState extends State<HomeRegisterScreen> {

  String myName = "N/A";
  String myLastname = "N/A";
  int myAge = 21;
  String myIdCard = "N/A";
  String myPhoneNumber = "N/A";

  Future readUsername() async {
    final user = await UserSecureStorage.getUser();

    setState(() {
      myName = user["name"] ?? "error name";
      myLastname = user["lastname"] ?? "error lastname";
      myAge = user["age"] ?? 21;
      myIdCard = user["IdCard"] ?? "error IdCard";
      myPhoneNumber = user["phone"] ?? "error phoneNumber";
    });
  }

  @override
  void initState() {
    readUsername();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
              onPressed: () {
                // TODO
              },
              icon: const Icon(
                Icons.person,
                color: Colors.white,
              )),
        title: const Text('Data From Register'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(14.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: const Color.fromARGB(255, 0, 15, 159),
              ),
              onPressed: () {
                onLogout();
              },
              child: const Text('Logout'),
            ),
          ),
          Expanded(child: Consumer(
            builder: (context, ProfileProvider provider, Widget? child) {
              return ListView.builder(
                  itemCount: 1,
                  itemBuilder: (context, int index) {
                    return Card(
                      elevation: 0.0,
                      child: ListTile(
                        leading:  CircleAvatar(
                          child: FittedBox(
                            child: Text('$myAge'), // age
                          ),
                        ),
                        title: Text('ชื่อ $myName'),
                        subtitle:  Text('หมายเลขโทรศัพท์ $myPhoneNumber'),
                      ),
                    );
                  });
            },
          )),
          const ThaiUniversityWidget(),
        ],
      )
    );
  }

  void onLogout() async {
    await UserSecureStorage.deleteUser();
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => const RegisterScreen()));
  }
}

