import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thai_university/screen/register_screen.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext Context) =>RegisterScreen()));          },
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
        title: Text('HomeScreen'),
        // backgroundColor: Colors.white,
        // elevation: 0,
      ),
      body: Container(
          // decoration: const BoxDecoration(color: Color.fromARGB(255, 255, 209, 0)),
          child: Padding(
              padding: EdgeInsets.all(15),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    child: TextFormField(
                      decoration: InputDecoration(fillColor: Colors.white, filled:true,
                        // hintText: 'Enter your Fristname',
                        labelText: 'กรอกชื่อ',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: const BorderSide(color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                  FlatButton(
                    child: Text("Add Data"),
                    color: Colors.blueAccent,
                    textColor: Colors.white,
                    onPressed: (){
                      // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext Context) =>RegisterScreen()));
                    },
                  ),
                ],
              ),
            ),
            ),
      )
    );
  }
}
