import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:thai_university/models/Thai_University_from_json.dart';

class ThaiUniversityWidget extends StatefulWidget {
  const ThaiUniversityWidget({Key? key}) : super(key: key);

  @override
  State<ThaiUniversityWidget> createState() => _ThaiUniversityState();
}

class _ThaiUniversityState extends State<ThaiUniversityWidget> {
  List<ThaiUniversity> data = [];

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(
      children: [
        const Text("University"),
        data.isEmpty
            ? const Text("No University")
            : Expanded(
                child: ListView.builder(
                    itemCount: data.length,
                    itemBuilder: (context, int index) {
                      ThaiUniversity university = data[index];
                      return Card(
                        elevation: 0.0,
                        child: ListTile(
                          leading: CircleAvatar(
                            child: FittedBox(
                              child: Text(university.alphaTwoCode),
                            ),
                          ),
                          title: Text("ชื่อ ${university.name}"),
                          subtitle: Text("ประเทศ ${university.country}"),
                        ),
                      );
                    }))
      ],
    ));
  }

  @override
  void initState() {
    super.initState();
    getExistingRate();
  }

  Future<void> getExistingRate() async {
    var url = "http://universities.hipolabs.com/search?country=Thailand";
    var response = await http.get(Uri.parse(url));
    List<ThaiUniversity> res = thaiUniversityFromJson(response.body);
    setState(() {
      data = res;
    });
    print("core response ${response.statusCode} ${response.body}");
  }
}
