import 'dart:collection';
import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class UserSecureStorage {
  static const _storage = FlutterSecureStorage();

  static const _keyUser = 'user';

  static Future setUser(HashMap user) async  {
    String userJson = json.encode(user);
    await _storage.write(key: _keyUser, value: userJson );
  }


  static Future<HashMap> getUser() async {
    var username = await _storage.read(key: _keyUser);
    HashMap hashMap = HashMap.from(json.decode(username ?? "{}"));
    return hashMap;

  }

  static Future deleteUser() async {
    await _storage.delete(key: _keyUser,);
  }



}