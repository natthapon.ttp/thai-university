import 'package:flutter/foundation.dart';
import 'package:thai_university/models/Profile.dart';

class ProfileProvider with ChangeNotifier {
      // ชื่อ นามสกุล อายุ เลขบัตรประชาชน หมายเลขโทรศัพท์
      // ตัวอย่างข้อมูล
      List<Profile> Profiles = [
        Profile(name: "สมชาย", lastname: "ใจดี", age: 19, idcard: 1500123214321,phonenumber: 0861112341),
        Profile(name: "สมหญิง", lastname: "ใจงาม", age: 21, idcard: 15001232143111,phonenumber: 0861112222)
      ];

      // ดึงข้อมูล
      List<Profile> getProfile(){
        return Profiles;
      }

      void addProfile(Profile detail){
        Profiles.add(detail);
      }
}