import 'dart:convert';

List<ThaiUniversity> thaiUniversityFromJson(String str) => List<ThaiUniversity>.from(json.decode(str).map((x) => ThaiUniversity.fromJson(x)));

String thaiUniversityToJson(List<ThaiUniversity> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ThaiUniversity {
  ThaiUniversity({
    required this.country,
    required this.domains,
    required this.webPages,
    required this.alphaTwoCode,
    required this.name,
    required this.stateProvince,
  });

  String country;
  List<String> domains;
  List<String> webPages;
  String alphaTwoCode;
  String name;
  String stateProvince;

  factory ThaiUniversity.fromJson(Map<String, dynamic> json) => ThaiUniversity(
    country: json["country"] ?? "",
    domains: json["domains"].map((x) => x),
    webPages: json["web_pages"].map((x) => x),
    alphaTwoCode: json["alpha_two_code"] ?? "",
    name: json["name"] ?? "",
    stateProvince: json["state-province"] ?? "",
  );

  Map<String, dynamic> toJson() => {
    "country": country ,
    "domains": List<dynamic>.from(domains.map((x) => x)),
    "web_pages": List<dynamic>.from(webPages.map((x) => x)),
    "alpha_two_code": alphaTwoCode ,
    "name": name,
    "state-province": stateProvince ,
  };
}

