class Profile {
  late String name;
  late String lastname;
  late double age;
  late double idcard;
  late double phonenumber;

  Profile({required this.name, required this.lastname, required this.age, required this.idcard, required this.phonenumber});
}